﻿using System.Collections.Generic;
using System.Security.Claims;
using IdentityModel;
using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;

namespace WebAppAuthServer
{
    public class Config
    {
        public static IEnumerable<Client> GetClients()
        {
            return new List<Client>
            {
                new Client
                {
                    ClientId = "892CACB8E9CE46AEA32DC7B7A7F39E39",
                    ClientSecrets = new List<Secret>
                    {
                        new Secret("345622A557F546959267ED8B3F6293B6".ToSha256())
                    },
                    AccessTokenType = AccessTokenType.Jwt,
                    AllowedGrantTypes = GrantTypes.ResourceOwnerPassword,
                    AllowedScopes = new List<string>
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "SampleApi",
                        "offline_access"
                    },
                    AllowOfflineAccess = true
                },
                //new Client
                //{
                //    ClientId = "892CACB8E9CE46AEA32DC7B7A7F39E39",
                //    ClientName = "MVC Client",
                //    ClientSecrets = {new Secret("3FEF2A32D1A14EFCA97EC0D341770206".Sha256())},

                //    AllowedGrantTypes = GrantTypes.CodeAndClientCredentials,
                //    RequirePkce = true,

                //    // where to redirect to after login
                //    RedirectUris =
                //    {
                //        "http://mvcclient.loveyday.com/signin-oidc", "http://mvcclient.poetrysharing.com/signin-oidc",
                //        "http://localhost:8283/signin-oidc"
                //    },

                //    FrontChannelLogoutUri = "http://mvcclient.loveyday.com/signin-oidc",

                //    // where to redirect to after logout
                //    PostLogoutRedirectUris =
                //    {
                //        "http://mvcclient.loveyday.com/signout-callback-oidc", "http://mvcclient.poetrysharing.com/signout-callback-oidc",
                //        "http://localhost:8283/signout-callback-oidc"
                //    },

                //    AccessTokenLifetime = 300,
                //    AllowedScopes = new List<string>
                //    {
                //        IdentityServerConstants.StandardScopes.OpenId,
                //        IdentityServerConstants.StandardScopes.Profile,
                //        "SampleApi",
                //        "offline_access"
                //    },
                //    AllowOfflineAccess = true
                //},
                //new Client
                //{
                //    ClientId = "345622A557F546959267ED8B3F6293B6",
                //    ClientName = "JavaScript or SPA Client",
                //    AllowedGrantTypes = GrantTypes.Code,
                //    RequirePkce = true,
                //    RequireClientSecret = false,

                //    RedirectUris =
                //    {
                //        "https://localhost:44382/callback", "http://localhost:44357/callback.html",
                //        "http://vueclient.poetrysharing.com/callback.html",
                //        "http://vueclient.loveyday.com/callback.html","http://localhost:4200/auth-callback"
                //    },
                //    PostLogoutRedirectUris =
                //    {
                //        "https://localhost:44382/index", "http://localhost:44357", "http://vueclient.poetrysharing.com",
                //        "http://vueclient.loveyday.com","http://localhost:4200"
                //    },
                //    AllowedCorsOrigins =
                //    {
                //        "https://localhost:44382", "http://localhost:44357", "http://vueclient.poetrysharing.com",
                //        "http://vueclient.loveyday.com","http://localhost:4200"
                //    },

                //    AllowedScopes =
                //    {
                //        IdentityServerConstants.StandardScopes.OpenId,
                //        IdentityServerConstants.StandardScopes.Profile,
                //        "SampleApi"
                //    }
                //}
            };
        }

        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile(),
                new IdentityResources.Email()
            };
        }

        public static IEnumerable<ApiResource> GetApiResources()
        {
            return new List<ApiResource>
            {
                new ApiResource("SampleApi", "My Sample API", new List<string>
                {
                    JwtClaimTypes.Name,
                    JwtClaimTypes.Email,
                    JwtClaimTypes.Role,
                    "JwtRole"
                })
            };
        }

        public static List<TestUser> GetUsers()
        {
            return new List<TestUser>
            {
                new TestUser
                {
                    SubjectId = "1",
                    Username = "Olek",
                    Password = "*IdentityServer*",
                    Claims = new List<Claim>
                    {
                        new Claim(JwtClaimTypes.Name,"Olek-Hzq"),
                        new Claim(JwtClaimTypes.Email,"875755898@qq.com"),
                        new Claim(JwtClaimTypes.Role,"Admin"),
                        new Claim("JwtRole","JwtAdmin")
                    }
                }
            };
        }
    }
}
