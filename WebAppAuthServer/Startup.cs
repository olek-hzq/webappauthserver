using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using WebAppAuthServer.Csp;

namespace WebAppAuthServer
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers(config => { config.EnableEndpointRouting = false; });

            services.AddIdentityServer().AddInMemoryClients(Config.GetClients())
                .AddInMemoryIdentityResources(Config.GetIdentityResources())
                .AddInMemoryApiResources(Config.GetApiResources())
                .AddTestUsers(Config.GetUsers())
                .AddDeveloperSigningCredential();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();

            app.UseHttpsRedirection();

            app.UseIdentityServer();
            app.UseCsp(builder =>
            {
                builder.Defaults
                    .AllowSelf()
                    .AllowAny();

                builder.Styles
                    .AllowSelf()
                    .Allow("'unsafe-inline'")
                    .AllowAny();

                builder.Scripts
                    .AllowSelf()
                    .Allow("'unsafe-inline'")
                    .Allow("https://unpkg.com")
                    .Allow("https://eqcn.ajz.miesnfu.com")
                    .Allow("https://cdn.bootcdn.net");
            });

            app.UseMvcWithDefaultRoute();
        }
    }
}
