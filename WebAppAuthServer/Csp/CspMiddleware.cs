﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace WebAppAuthServer.Csp
{
    public class CspMiddleware
    {
        private const string Header = "Content-Security-Policy";
        private readonly RequestDelegate _next;
        private readonly CspOptions _options;

        public CspMiddleware(
            RequestDelegate next, CspOptions options)
        {
            _next = next;
            _options = options;
        }

        public async Task Invoke(HttpContext context)
        {
            context.Response.Headers.Add(Header, GetHeaderValue());
            await _next(context);
        }

        private string GetHeaderValue()
        {
            var value = "";
            value += GetDirective("default-src", _options.Defaults);
            value += GetDirective("script-src", _options.Scripts);
            value += GetDirective("style-src", _options.Styles);
            value += GetDirective("img-src", _options.Images);
            value += GetDirective("font-src", _options.Fonts);
            value += GetDirective("media-src", _options.Media);
            return value;
        }

        private static string GetDirective(string directive, List<string> sources)
            => sources.Count > 0 ? $"{directive} {string.Join(" ", sources)}; " : "";
    }
}
