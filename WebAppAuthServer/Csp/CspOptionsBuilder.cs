﻿namespace WebAppAuthServer.Csp
{
    public class CspOptionsBuilder
    {
        private readonly CspOptions _options = new CspOptions();

        internal CspOptionsBuilder() { }

        public CspDirectiveBuilder Defaults { get; set; } = new CspDirectiveBuilder();
        public CspDirectiveBuilder Scripts { get; set; } = new CspDirectiveBuilder();
        public CspDirectiveBuilder Styles { get; set; } = new CspDirectiveBuilder();
        public CspDirectiveBuilder Images { get; set; } = new CspDirectiveBuilder();
        public CspDirectiveBuilder Fonts { get; set; } = new CspDirectiveBuilder();
        public CspDirectiveBuilder Media { get; set; } = new CspDirectiveBuilder();

        internal CspOptions Build()
        {
            _options.Defaults = Defaults.Sources;
            _options.Scripts = Scripts.Sources;
            _options.Styles = Styles.Sources;
            _options.Images = Images.Sources;
            _options.Fonts = Fonts.Sources;
            _options.Media = Media.Sources;
            return _options;
        }
    }
}
